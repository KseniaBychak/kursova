﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FractLib;
using AppendixLibrary;

namespace Fractals
{
    public partial class Julia : Form
    {
        public Julia()
        {
            InitializeComponent();
        }
        private void ButtonColorScheme_Click(object sender, EventArgs e)
        {
            colorDialog.AllowFullOpen = false;
            colorDialog.ShowHelp = true;
            colorDialog.Color = ButtonColorScheme.BackColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
                ButtonColorScheme.BackColor = colorDialog.Color;
            Draw();
        }
        private void NumBoxReal_ValueChanged(object sender, EventArgs e)
        {
            Draw();
        }
        private void Draw()
        {
            Bitmap drawArea = new Bitmap(PictureBoxSet.Width, PictureBoxSet.Height);
            Graphics g = Graphics.FromImage(drawArea);
            JuliaSet julia = new JuliaSet(ButtonColorScheme.BackColor, new Complex((double)NumericUpDownReal.Value, (double)NumericUpDownIm.Value),new Size(drawArea.Width, drawArea.Height));
            julia.Building(g);
            PictureBoxSet.Image = drawArea;
        }

        private void NumBoxIm_ValueChanged(object sender, EventArgs e)
        {
            Draw();
        }

        private void Julia_Load(object sender, EventArgs e)
        {
            Draw();
        }
    }
}
