﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FractLib;

namespace Fractals
{
    public partial class SnowflakeKoch : Form
    {
        public SnowflakeKoch()
        {
            InitializeComponent();
        }
        private void ButtonColor_Click(object sender, EventArgs e)
        {
            ColorDialog.AllowFullOpen = false;
            ColorDialog.ShowHelp = true;
            ColorDialog.Color = ButtonColor.BackColor;
            if (ColorDialog.ShowDialog() == DialogResult.OK)
                ButtonColor.BackColor = ColorDialog.Color;
        }
        private void ButtonBuild_Click(object sender, EventArgs e)
        {
            Bitmap drawArea = new Bitmap(PictureBoxKoch.Width, PictureBoxKoch.Height);
            PictureBoxKoch.Image = drawArea;
            Graphics g = Graphics.FromImage(drawArea);
            PointF center = new PointF((int)(NumericUpDownLeft.Value+NumericUpDownLength.Value/2+10), (int)(NumericUpDownTop.Value + NumericUpDownLength.Value* (decimal)(Math.Sqrt(3) / 2) / 3 * 2));
            PointF[] points = new PointF[(int)(3*Math.Pow(4,(double)NumericUpDownIteration.Value))];
            float x, y;
            y = (int)(center.Y - (double)NumericUpDownLength.Value * (Math.Sqrt(3) / 2) / 3 * 2);
            points[0]=new PointF(center.X,y);
            y = (float)(center.Y + (float)NumericUpDownLength.Value * (Math.Sqrt(3) / 2.0) / 3.0);
            x = (float)(center.X + (float)NumericUpDownLength.Value / 2.0);
            points[1] = new PointF(x, y);
            x = (float)(center.X - (float)NumericUpDownLength.Value / 2.0);
            points[2] = new PointF(x, y);
            Snowflake snowflake = new Snowflake(points, ButtonColor.BackColor, (int)NumericUpDownIteration.Value, (int)NumericUpDownLength.Value,RadioButtonFill.Checked,CheckBoxAnti.Checked);
            snowflake.Building(g);
            PictureBoxKoch.Image = drawArea;
        }
    }
}
