﻿namespace Fractals
{
    partial class FernBarnsley
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelColor = new System.Windows.Forms.Label();
            this.ButtonColor = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.LabelAfunc = new System.Windows.Forms.Label();
            this.ButtonBuild = new System.Windows.Forms.Button();
            this.IterationLabel = new System.Windows.Forms.Label();
            this.NumericUpDownIter = new System.Windows.Forms.NumericUpDown();
            this.ButtonStartOptions = new System.Windows.Forms.Button();
            this.PictureBoxFern = new System.Windows.Forms.PictureBox();
            this.TrackBarStemLeaf = new System.Windows.Forms.TrackBar();
            this.LabelStemLeaf = new System.Windows.Forms.Label();
            this.LabelUp = new System.Windows.Forms.Label();
            this.LabelDown = new System.Windows.Forms.Label();
            this.LabelRound = new System.Windows.Forms.Label();
            this.LabelSharp = new System.Windows.Forms.Label();
            this.LabelLeaf = new System.Windows.Forms.Label();
            this.TrackBarLeafShape = new System.Windows.Forms.TrackBar();
            this.LabelDown2 = new System.Windows.Forms.Label();
            this.LabelUp2 = new System.Windows.Forms.Label();
            this.LabelVertIncline = new System.Windows.Forms.Label();
            this.TrackBarVertIncline = new System.Windows.Forms.TrackBar();
            this.LabelBottom = new System.Windows.Forms.Label();
            this.LabelTop = new System.Windows.Forms.Label();
            this.LabelTurn = new System.Windows.Forms.Label();
            this.TrackBarTurn = new System.Windows.Forms.TrackBar();
            this.LabelUnfolded = new System.Windows.Forms.Label();
            this.LabelBent = new System.Windows.Forms.Label();
            this.LabelTension = new System.Windows.Forms.Label();
            this.TrackBarTension = new System.Windows.Forms.TrackBar();
            this.LabelBig = new System.Windows.Forms.Label();
            this.LabelSmall = new System.Windows.Forms.Label();
            this.LabelSize = new System.Windows.Forms.Label();
            this.TrackBarSize = new System.Windows.Forms.TrackBar();
            this.LabelTopLeaf = new System.Windows.Forms.Label();
            this.LabelDownLeaf = new System.Windows.Forms.Label();
            this.LabelRotateLeaf = new System.Windows.Forms.Label();
            this.TrackBarRotateTop = new System.Windows.Forms.TrackBar();
            this.LabelConvolution = new System.Windows.Forms.Label();
            this.LabelUnfoldedTop = new System.Windows.Forms.Label();
            this.LabelBentTop = new System.Windows.Forms.Label();
            this.TrackBarTensionTop = new System.Windows.Forms.TrackBar();
            this.LabelWideTop = new System.Windows.Forms.Label();
            this.LabelNarrowTop = new System.Windows.Forms.Label();
            this.TrackBarWidthTop = new System.Windows.Forms.TrackBar();
            this.LabelWidth = new System.Windows.Forms.Label();
            this.LabelAcuteTop = new System.Windows.Forms.Label();
            this.LabelObtuseTop = new System.Windows.Forms.Label();
            this.TrackBarAngleTop = new System.Windows.Forms.TrackBar();
            this.LabelAngleTop = new System.Windows.Forms.Label();
            this.TrackBarDistanceTop = new System.Windows.Forms.TrackBar();
            this.LabelDistanceTop = new System.Windows.Forms.Label();
            this.TrackBarDistanceBottom = new System.Windows.Forms.TrackBar();
            this.LabelAcuteBottom = new System.Windows.Forms.Label();
            this.LabelObtuseBottom = new System.Windows.Forms.Label();
            this.TrackBarAngleBottom = new System.Windows.Forms.TrackBar();
            this.LabelWideBottom = new System.Windows.Forms.Label();
            this.LabelNarrowBottom = new System.Windows.Forms.Label();
            this.TrackBarWidthBottom = new System.Windows.Forms.TrackBar();
            this.LabelUnfoldedBottom = new System.Windows.Forms.Label();
            this.LabelBentBottom = new System.Windows.Forms.Label();
            this.TrackBarTensionBottom = new System.Windows.Forms.TrackBar();
            this.TrackBarRotateBottom = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarStemLeaf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarLeafShape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVertIncline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTurn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRotateTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTensionTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarWidthTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAngleTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDistanceTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDistanceBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAngleBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarWidthBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTensionBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRotateBottom)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelColor
            // 
            this.LabelColor.AutoSize = true;
            this.LabelColor.Font = new System.Drawing.Font("Bookman Old Style", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelColor.Location = new System.Drawing.Point(12, 9);
            this.LabelColor.Name = "LabelColor";
            this.LabelColor.Size = new System.Drawing.Size(82, 26);
            this.LabelColor.TabIndex = 0;
            this.LabelColor.Text = "Колір:";
            // 
            // ButtonColor
            // 
            this.ButtonColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ButtonColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonColor.Location = new System.Drawing.Point(100, 9);
            this.ButtonColor.Name = "ButtonColor";
            this.ButtonColor.Size = new System.Drawing.Size(30, 26);
            this.ButtonColor.TabIndex = 1;
            this.ButtonColor.UseVisualStyleBackColor = false;
            this.ButtonColor.Click += new System.EventHandler(this.ButtonColor_Click);
            // 
            // LabelAfunc
            // 
            this.LabelAfunc.AutoSize = true;
            this.LabelAfunc.Font = new System.Drawing.Font("Bookman Old Style", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAfunc.Location = new System.Drawing.Point(19, 55);
            this.LabelAfunc.Name = "LabelAfunc";
            this.LabelAfunc.Size = new System.Drawing.Size(510, 28);
            this.LabelAfunc.TabIndex = 2;
            this.LabelAfunc.Text = "Налаштування афінних перетворень:";
            // 
            // ButtonBuild
            // 
            this.ButtonBuild.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBuild.Location = new System.Drawing.Point(10, 643);
            this.ButtonBuild.Name = "ButtonBuild";
            this.ButtonBuild.Size = new System.Drawing.Size(548, 42);
            this.ButtonBuild.TabIndex = 35;
            this.ButtonBuild.Text = "Побудувати";
            this.ButtonBuild.UseVisualStyleBackColor = true;
            this.ButtonBuild.Click += new System.EventHandler(this.ButtonBuild_Click);
            // 
            // IterationLabel
            // 
            this.IterationLabel.AutoSize = true;
            this.IterationLabel.Font = new System.Drawing.Font("Bookman Old Style", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IterationLabel.Location = new System.Drawing.Point(169, 9);
            this.IterationLabel.Name = "IterationLabel";
            this.IterationLabel.Size = new System.Drawing.Size(226, 26);
            this.IterationLabel.TabIndex = 36;
            this.IterationLabel.Text = "Кількість ітерацій:";
            // 
            // NumericUpDownIter
            // 
            this.NumericUpDownIter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownIter.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumericUpDownIter.Location = new System.Drawing.Point(429, 9);
            this.NumericUpDownIter.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.NumericUpDownIter.Minimum = new decimal(new int[] {
            7000,
            0,
            0,
            0});
            this.NumericUpDownIter.Name = "NumericUpDownIter";
            this.NumericUpDownIter.Size = new System.Drawing.Size(93, 27);
            this.NumericUpDownIter.TabIndex = 37;
            this.NumericUpDownIter.Value = new decimal(new int[] {
            18000,
            0,
            0,
            0});
            // 
            // ButtonStartOptions
            // 
            this.ButtonStartOptions.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonStartOptions.Location = new System.Drawing.Point(12, 86);
            this.ButtonStartOptions.Name = "ButtonStartOptions";
            this.ButtonStartOptions.Size = new System.Drawing.Size(553, 42);
            this.ButtonStartOptions.TabIndex = 38;
            this.ButtonStartOptions.Text = "Встановити по замовчуванню";
            this.ButtonStartOptions.UseVisualStyleBackColor = true;
            this.ButtonStartOptions.Click += new System.EventHandler(this.ButtonStartOptions_Click);
            // 
            // PictureBoxFern
            // 
            this.PictureBoxFern.Location = new System.Drawing.Point(583, 13);
            this.PictureBoxFern.Name = "PictureBoxFern";
            this.PictureBoxFern.Size = new System.Drawing.Size(850, 678);
            this.PictureBoxFern.TabIndex = 39;
            this.PictureBoxFern.TabStop = false;
            // 
            // TrackBarStemLeaf
            // 
            this.TrackBarStemLeaf.Location = new System.Drawing.Point(324, 133);
            this.TrackBarStemLeaf.Maximum = 5;
            this.TrackBarStemLeaf.Minimum = -5;
            this.TrackBarStemLeaf.Name = "TrackBarStemLeaf";
            this.TrackBarStemLeaf.Size = new System.Drawing.Size(205, 56);
            this.TrackBarStemLeaf.TabIndex = 40;
            // 
            // LabelStemLeaf
            // 
            this.LabelStemLeaf.AutoSize = true;
            this.LabelStemLeaf.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelStemLeaf.Location = new System.Drawing.Point(8, 143);
            this.LabelStemLeaf.Name = "LabelStemLeaf";
            this.LabelStemLeaf.Size = new System.Drawing.Size(276, 22);
            this.LabelStemLeaf.TabIndex = 41;
            this.LabelStemLeaf.Text = "Перетворення стебла в листки:";
            // 
            // LabelUp
            // 
            this.LabelUp.AutoSize = true;
            this.LabelUp.Location = new System.Drawing.Point(290, 147);
            this.LabelUp.Name = "LabelUp";
            this.LabelUp.Size = new System.Drawing.Size(50, 32);
            this.LabelUp.TabIndex = 42;
            this.LabelUp.Text = "Нахил \r\nвгору";
            // 
            // LabelDown
            // 
            this.LabelDown.AutoSize = true;
            this.LabelDown.Location = new System.Drawing.Point(518, 147);
            this.LabelDown.Name = "LabelDown";
            this.LabelDown.Size = new System.Drawing.Size(50, 32);
            this.LabelDown.TabIndex = 43;
            this.LabelDown.Text = "Нахил \r\nвниз";
            // 
            // LabelRound
            // 
            this.LabelRound.AutoSize = true;
            this.LabelRound.Location = new System.Drawing.Point(511, 195);
            this.LabelRound.Name = "LabelRound";
            this.LabelRound.Size = new System.Drawing.Size(62, 16);
            this.LabelRound.TabIndex = 47;
            this.LabelRound.Text = "Округле";
            // 
            // LabelSharp
            // 
            this.LabelSharp.AutoSize = true;
            this.LabelSharp.Location = new System.Drawing.Point(283, 195);
            this.LabelSharp.Name = "LabelSharp";
            this.LabelSharp.Size = new System.Drawing.Size(52, 16);
            this.LabelSharp.TabIndex = 46;
            this.LabelSharp.Text = "Гостре";
            // 
            // LabelLeaf
            // 
            this.LabelLeaf.AutoSize = true;
            this.LabelLeaf.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelLeaf.Location = new System.Drawing.Point(150, 181);
            this.LabelLeaf.Name = "LabelLeaf";
            this.LabelLeaf.Size = new System.Drawing.Size(127, 22);
            this.LabelLeaf.TabIndex = 45;
            this.LabelLeaf.Text = "Форма листя:";
            // 
            // TrackBarLeafShape
            // 
            this.TrackBarLeafShape.Location = new System.Drawing.Point(317, 181);
            this.TrackBarLeafShape.Maximum = 90;
            this.TrackBarLeafShape.Minimum = 77;
            this.TrackBarLeafShape.Name = "TrackBarLeafShape";
            this.TrackBarLeafShape.Size = new System.Drawing.Size(205, 56);
            this.TrackBarLeafShape.TabIndex = 44;
            this.TrackBarLeafShape.Value = 85;
            // 
            // LabelDown2
            // 
            this.LabelDown2.AutoSize = true;
            this.LabelDown2.Location = new System.Drawing.Point(510, 223);
            this.LabelDown2.Name = "LabelDown2";
            this.LabelDown2.Size = new System.Drawing.Size(50, 32);
            this.LabelDown2.TabIndex = 51;
            this.LabelDown2.Text = "Нахил \r\nвниз";
            // 
            // LabelUp2
            // 
            this.LabelUp2.AutoSize = true;
            this.LabelUp2.Location = new System.Drawing.Point(283, 223);
            this.LabelUp2.Name = "LabelUp2";
            this.LabelUp2.Size = new System.Drawing.Size(50, 32);
            this.LabelUp2.TabIndex = 50;
            this.LabelUp2.Text = "Нахил \r\nвгору";
            // 
            // LabelVertIncline
            // 
            this.LabelVertIncline.AutoSize = true;
            this.LabelVertIncline.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelVertIncline.Location = new System.Drawing.Point(2, 226);
            this.LabelVertIncline.Name = "LabelVertIncline";
            this.LabelVertIncline.Size = new System.Drawing.Size(275, 22);
            this.LabelVertIncline.TabIndex = 49;
            this.LabelVertIncline.Text = "Вертикальний нахил папороті:";
            // 
            // TrackBarVertIncline
            // 
            this.TrackBarVertIncline.Location = new System.Drawing.Point(317, 216);
            this.TrackBarVertIncline.Maximum = 90;
            this.TrackBarVertIncline.Minimum = -45;
            this.TrackBarVertIncline.Name = "TrackBarVertIncline";
            this.TrackBarVertIncline.Size = new System.Drawing.Size(205, 56);
            this.TrackBarVertIncline.TabIndex = 48;
            this.TrackBarVertIncline.Value = 40;
            // 
            // LabelBottom
            // 
            this.LabelBottom.AutoSize = true;
            this.LabelBottom.Location = new System.Drawing.Point(506, 258);
            this.LabelBottom.Name = "LabelBottom";
            this.LabelBottom.Size = new System.Drawing.Size(57, 32);
            this.LabelBottom.TabIndex = 55;
            this.LabelBottom.Text = "Ближче\r\nниз";
            // 
            // LabelTop
            // 
            this.LabelTop.AutoSize = true;
            this.LabelTop.Location = new System.Drawing.Point(283, 258);
            this.LabelTop.Name = "LabelTop";
            this.LabelTop.Size = new System.Drawing.Size(57, 32);
            this.LabelTop.TabIndex = 54;
            this.LabelTop.Text = "Ближче\r\nверх";
            // 
            // LabelTurn
            // 
            this.LabelTurn.AutoSize = true;
            this.LabelTurn.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTurn.Location = new System.Drawing.Point(108, 268);
            this.LabelTurn.Name = "LabelTurn";
            this.LabelTurn.Size = new System.Drawing.Size(169, 22);
            this.LabelTurn.TabIndex = 53;
            this.LabelTurn.Text = "Поворот папороті:";
            // 
            // TrackBarTurn
            // 
            this.TrackBarTurn.Location = new System.Drawing.Point(316, 258);
            this.TrackBarTurn.Minimum = -20;
            this.TrackBarTurn.Name = "TrackBarTurn";
            this.TrackBarTurn.Size = new System.Drawing.Size(205, 56);
            this.TrackBarTurn.TabIndex = 52;
            this.TrackBarTurn.Value = -4;
            // 
            // LabelUnfolded
            // 
            this.LabelUnfolded.AutoSize = true;
            this.LabelUnfolded.Location = new System.Drawing.Point(495, 307);
            this.LabelUnfolded.Name = "LabelUnfolded";
            this.LabelUnfolded.Size = new System.Drawing.Size(72, 16);
            this.LabelUnfolded.TabIndex = 59;
            this.LabelUnfolded.Text = "Розігнута";
            // 
            // LabelBent
            // 
            this.LabelBent.AutoSize = true;
            this.LabelBent.Location = new System.Drawing.Point(282, 307);
            this.LabelBent.Name = "LabelBent";
            this.LabelBent.Size = new System.Drawing.Size(56, 16);
            this.LabelBent.TabIndex = 58;
            this.LabelBent.Text = "Зігнута";
            // 
            // LabelTension
            // 
            this.LabelTension.AutoSize = true;
            this.LabelTension.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTension.Location = new System.Drawing.Point(60, 301);
            this.LabelTension.Name = "LabelTension";
            this.LabelTension.Size = new System.Drawing.Size(217, 22);
            this.LabelTension.TabIndex = 57;
            this.LabelTension.Text = "Напруженість папороті:";
            // 
            // TrackBarTension
            // 
            this.TrackBarTension.Location = new System.Drawing.Point(316, 293);
            this.TrackBarTension.Maximum = 90;
            this.TrackBarTension.Minimum = 70;
            this.TrackBarTension.Name = "TrackBarTension";
            this.TrackBarTension.Size = new System.Drawing.Size(205, 56);
            this.TrackBarTension.TabIndex = 56;
            this.TrackBarTension.Value = 85;
            // 
            // LabelBig
            // 
            this.LabelBig.AutoSize = true;
            this.LabelBig.Location = new System.Drawing.Point(510, 340);
            this.LabelBig.Name = "LabelBig";
            this.LabelBig.Size = new System.Drawing.Size(55, 16);
            this.LabelBig.TabIndex = 63;
            this.LabelBig.Text = "Велика";
            // 
            // LabelSmall
            // 
            this.LabelSmall.AutoSize = true;
            this.LabelSmall.Location = new System.Drawing.Point(282, 340);
            this.LabelSmall.Name = "LabelSmall";
            this.LabelSmall.Size = new System.Drawing.Size(72, 16);
            this.LabelSmall.TabIndex = 62;
            this.LabelSmall.Text = "Маленька";
            // 
            // LabelSize
            // 
            this.LabelSize.AutoSize = true;
            this.LabelSize.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelSize.Location = new System.Drawing.Point(122, 336);
            this.LabelSize.Name = "LabelSize";
            this.LabelSize.Size = new System.Drawing.Size(155, 22);
            this.LabelSize.TabIndex = 61;
            this.LabelSize.Text = "Розмір папороті:";
            // 
            // TrackBarSize
            // 
            this.TrackBarSize.Location = new System.Drawing.Point(316, 326);
            this.TrackBarSize.Maximum = 20;
            this.TrackBarSize.Minimum = 12;
            this.TrackBarSize.Name = "TrackBarSize";
            this.TrackBarSize.Size = new System.Drawing.Size(205, 56);
            this.TrackBarSize.TabIndex = 60;
            this.TrackBarSize.Value = 16;
            // 
            // LabelTopLeaf
            // 
            this.LabelTopLeaf.AutoSize = true;
            this.LabelTopLeaf.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTopLeaf.Location = new System.Drawing.Point(150, 388);
            this.LabelTopLeaf.Name = "LabelTopLeaf";
            this.LabelTopLeaf.Size = new System.Drawing.Size(124, 22);
            this.LabelTopLeaf.TabIndex = 64;
            this.LabelTopLeaf.Text = "Верхнє листя";
            // 
            // LabelDownLeaf
            // 
            this.LabelDownLeaf.AutoSize = true;
            this.LabelDownLeaf.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelDownLeaf.Location = new System.Drawing.Point(383, 388);
            this.LabelDownLeaf.Name = "LabelDownLeaf";
            this.LabelDownLeaf.Size = new System.Drawing.Size(122, 22);
            this.LabelDownLeaf.TabIndex = 65;
            this.LabelDownLeaf.Text = "Нижнє листя";
            // 
            // LabelRotateLeaf
            // 
            this.LabelRotateLeaf.AutoSize = true;
            this.LabelRotateLeaf.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelRotateLeaf.Location = new System.Drawing.Point(6, 428);
            this.LabelRotateLeaf.Name = "LabelRotateLeaf";
            this.LabelRotateLeaf.Size = new System.Drawing.Size(141, 22);
            this.LabelRotateLeaf.TabIndex = 66;
            this.LabelRotateLeaf.Text = "Поворот листя:";
            // 
            // TrackBarRotateTop
            // 
            this.TrackBarRotateTop.Location = new System.Drawing.Point(164, 415);
            this.TrackBarRotateTop.Maximum = 50;
            this.TrackBarRotateTop.Name = "TrackBarRotateTop";
            this.TrackBarRotateTop.Size = new System.Drawing.Size(120, 56);
            this.TrackBarRotateTop.TabIndex = 67;
            this.TrackBarRotateTop.Value = 20;
            // 
            // LabelConvolution
            // 
            this.LabelConvolution.AutoSize = true;
            this.LabelConvolution.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelConvolution.Location = new System.Drawing.Point(6, 468);
            this.LabelConvolution.Name = "LabelConvolution";
            this.LabelConvolution.Size = new System.Drawing.Size(121, 22);
            this.LabelConvolution.TabIndex = 70;
            this.LabelConvolution.Text = "Складеність:";
            // 
            // LabelUnfoldedTop
            // 
            this.LabelUnfoldedTop.AutoSize = true;
            this.LabelUnfoldedTop.Location = new System.Drawing.Point(264, 470);
            this.LabelUnfoldedTop.Name = "LabelUnfoldedTop";
            this.LabelUnfoldedTop.Size = new System.Drawing.Size(72, 16);
            this.LabelUnfoldedTop.TabIndex = 73;
            this.LabelUnfoldedTop.Text = "Розігнуте";
            // 
            // LabelBentTop
            // 
            this.LabelBentTop.AutoSize = true;
            this.LabelBentTop.Location = new System.Drawing.Point(125, 470);
            this.LabelBentTop.Name = "LabelBentTop";
            this.LabelBentTop.Size = new System.Drawing.Size(56, 16);
            this.LabelBentTop.TabIndex = 72;
            this.LabelBentTop.Text = "Зігнуте";
            // 
            // TrackBarTensionTop
            // 
            this.TrackBarTensionTop.Location = new System.Drawing.Point(157, 457);
            this.TrackBarTensionTop.Maximum = -10;
            this.TrackBarTensionTop.Minimum = -40;
            this.TrackBarTensionTop.Name = "TrackBarTensionTop";
            this.TrackBarTensionTop.Size = new System.Drawing.Size(120, 56);
            this.TrackBarTensionTop.TabIndex = 71;
            this.TrackBarTensionTop.Value = -26;
            // 
            // LabelWideTop
            // 
            this.LabelWideTop.AutoSize = true;
            this.LabelWideTop.Location = new System.Drawing.Point(267, 514);
            this.LabelWideTop.Name = "LabelWideTop";
            this.LabelWideTop.Size = new System.Drawing.Size(57, 16);
            this.LabelWideTop.TabIndex = 77;
            this.LabelWideTop.Text = "Широке";
            // 
            // LabelNarrowTop
            // 
            this.LabelNarrowTop.AutoSize = true;
            this.LabelNarrowTop.Location = new System.Drawing.Point(128, 514);
            this.LabelNarrowTop.Name = "LabelNarrowTop";
            this.LabelNarrowTop.Size = new System.Drawing.Size(54, 16);
            this.LabelNarrowTop.TabIndex = 76;
            this.LabelNarrowTop.Text = "Вузьке";
            // 
            // TrackBarWidthTop
            // 
            this.TrackBarWidthTop.Location = new System.Drawing.Point(160, 501);
            this.TrackBarWidthTop.Maximum = 40;
            this.TrackBarWidthTop.Minimum = 10;
            this.TrackBarWidthTop.Name = "TrackBarWidthTop";
            this.TrackBarWidthTop.Size = new System.Drawing.Size(120, 56);
            this.TrackBarWidthTop.TabIndex = 75;
            this.TrackBarWidthTop.Value = 23;
            // 
            // LabelWidth
            // 
            this.LabelWidth.AutoSize = true;
            this.LabelWidth.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelWidth.Location = new System.Drawing.Point(9, 512);
            this.LabelWidth.Name = "LabelWidth";
            this.LabelWidth.Size = new System.Drawing.Size(88, 22);
            this.LabelWidth.TabIndex = 74;
            this.LabelWidth.Text = "Ширина:";
            // 
            // LabelAcuteTop
            // 
            this.LabelAcuteTop.AutoSize = true;
            this.LabelAcuteTop.Location = new System.Drawing.Point(267, 546);
            this.LabelAcuteTop.Name = "LabelAcuteTop";
            this.LabelAcuteTop.Size = new System.Drawing.Size(60, 16);
            this.LabelAcuteTop.TabIndex = 81;
            this.LabelAcuteTop.Text = "Гострий";
            // 
            // LabelObtuseTop
            // 
            this.LabelObtuseTop.AutoSize = true;
            this.LabelObtuseTop.Location = new System.Drawing.Point(128, 546);
            this.LabelObtuseTop.Name = "LabelObtuseTop";
            this.LabelObtuseTop.Size = new System.Drawing.Size(48, 16);
            this.LabelObtuseTop.TabIndex = 80;
            this.LabelObtuseTop.Text = "Тупий";
            // 
            // TrackBarAngleTop
            // 
            this.TrackBarAngleTop.Location = new System.Drawing.Point(160, 533);
            this.TrackBarAngleTop.Maximum = 45;
            this.TrackBarAngleTop.Minimum = -8;
            this.TrackBarAngleTop.Name = "TrackBarAngleTop";
            this.TrackBarAngleTop.Size = new System.Drawing.Size(120, 56);
            this.TrackBarAngleTop.TabIndex = 79;
            this.TrackBarAngleTop.Value = 22;
            // 
            // LabelAngleTop
            // 
            this.LabelAngleTop.AutoSize = true;
            this.LabelAngleTop.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAngleTop.Location = new System.Drawing.Point(9, 544);
            this.LabelAngleTop.Name = "LabelAngleTop";
            this.LabelAngleTop.Size = new System.Drawing.Size(109, 22);
            this.LabelAngleTop.TabIndex = 78;
            this.LabelAngleTop.Text = "Кут нахилу:";
            // 
            // TrackBarDistanceTop
            // 
            this.TrackBarDistanceTop.Location = new System.Drawing.Point(157, 581);
            this.TrackBarDistanceTop.Maximum = 290;
            this.TrackBarDistanceTop.Minimum = 4;
            this.TrackBarDistanceTop.Name = "TrackBarDistanceTop";
            this.TrackBarDistanceTop.Size = new System.Drawing.Size(120, 56);
            this.TrackBarDistanceTop.TabIndex = 83;
            this.TrackBarDistanceTop.Value = 16;
            // 
            // LabelDistanceTop
            // 
            this.LabelDistanceTop.AutoSize = true;
            this.LabelDistanceTop.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelDistanceTop.Location = new System.Drawing.Point(8, 581);
            this.LabelDistanceTop.Name = "LabelDistanceTop";
            this.LabelDistanceTop.Size = new System.Drawing.Size(143, 44);
            this.LabelDistanceTop.TabIndex = 82;
            this.LabelDistanceTop.Text = "Відстань до\r\nпочатку стебла:";
            // 
            // TrackBarDistanceBottom
            // 
            this.TrackBarDistanceBottom.Location = new System.Drawing.Point(385, 581);
            this.TrackBarDistanceBottom.Maximum = 290;
            this.TrackBarDistanceBottom.Minimum = 4;
            this.TrackBarDistanceBottom.Name = "TrackBarDistanceBottom";
            this.TrackBarDistanceBottom.Size = new System.Drawing.Size(120, 56);
            this.TrackBarDistanceBottom.TabIndex = 96;
            this.TrackBarDistanceBottom.Value = 44;
            // 
            // LabelAcuteBottom
            // 
            this.LabelAcuteBottom.AutoSize = true;
            this.LabelAcuteBottom.Location = new System.Drawing.Point(495, 546);
            this.LabelAcuteBottom.Name = "LabelAcuteBottom";
            this.LabelAcuteBottom.Size = new System.Drawing.Size(60, 16);
            this.LabelAcuteBottom.TabIndex = 95;
            this.LabelAcuteBottom.Text = "Гострий";
            // 
            // LabelObtuseBottom
            // 
            this.LabelObtuseBottom.AutoSize = true;
            this.LabelObtuseBottom.Location = new System.Drawing.Point(356, 546);
            this.LabelObtuseBottom.Name = "LabelObtuseBottom";
            this.LabelObtuseBottom.Size = new System.Drawing.Size(48, 16);
            this.LabelObtuseBottom.TabIndex = 94;
            this.LabelObtuseBottom.Text = "Тупий";
            // 
            // TrackBarAngleBottom
            // 
            this.TrackBarAngleBottom.Location = new System.Drawing.Point(388, 533);
            this.TrackBarAngleBottom.Maximum = 45;
            this.TrackBarAngleBottom.Name = "TrackBarAngleBottom";
            this.TrackBarAngleBottom.Size = new System.Drawing.Size(120, 56);
            this.TrackBarAngleBottom.TabIndex = 93;
            this.TrackBarAngleBottom.Value = 24;
            // 
            // LabelWideBottom
            // 
            this.LabelWideBottom.AutoSize = true;
            this.LabelWideBottom.Location = new System.Drawing.Point(495, 514);
            this.LabelWideBottom.Name = "LabelWideBottom";
            this.LabelWideBottom.Size = new System.Drawing.Size(57, 16);
            this.LabelWideBottom.TabIndex = 92;
            this.LabelWideBottom.Text = "Широке";
            // 
            // LabelNarrowBottom
            // 
            this.LabelNarrowBottom.AutoSize = true;
            this.LabelNarrowBottom.Location = new System.Drawing.Point(356, 514);
            this.LabelNarrowBottom.Name = "LabelNarrowBottom";
            this.LabelNarrowBottom.Size = new System.Drawing.Size(54, 16);
            this.LabelNarrowBottom.TabIndex = 91;
            this.LabelNarrowBottom.Text = "Вузьке";
            // 
            // TrackBarWidthBottom
            // 
            this.TrackBarWidthBottom.Location = new System.Drawing.Point(388, 501);
            this.TrackBarWidthBottom.Maximum = 45;
            this.TrackBarWidthBottom.Minimum = 4;
            this.TrackBarWidthBottom.Name = "TrackBarWidthBottom";
            this.TrackBarWidthBottom.Size = new System.Drawing.Size(120, 56);
            this.TrackBarWidthBottom.TabIndex = 90;
            this.TrackBarWidthBottom.Value = 26;
            // 
            // LabelUnfoldedBottom
            // 
            this.LabelUnfoldedBottom.AutoSize = true;
            this.LabelUnfoldedBottom.Location = new System.Drawing.Point(492, 470);
            this.LabelUnfoldedBottom.Name = "LabelUnfoldedBottom";
            this.LabelUnfoldedBottom.Size = new System.Drawing.Size(72, 16);
            this.LabelUnfoldedBottom.TabIndex = 89;
            this.LabelUnfoldedBottom.Text = "Розігнуте";
            // 
            // LabelBentBottom
            // 
            this.LabelBentBottom.AutoSize = true;
            this.LabelBentBottom.Location = new System.Drawing.Point(353, 470);
            this.LabelBentBottom.Name = "LabelBentBottom";
            this.LabelBentBottom.Size = new System.Drawing.Size(56, 16);
            this.LabelBentBottom.TabIndex = 88;
            this.LabelBentBottom.Text = "Зігнуте";
            // 
            // TrackBarTensionBottom
            // 
            this.TrackBarTensionBottom.Location = new System.Drawing.Point(385, 457);
            this.TrackBarTensionBottom.Maximum = 45;
            this.TrackBarTensionBottom.Minimum = 18;
            this.TrackBarTensionBottom.Name = "TrackBarTensionBottom";
            this.TrackBarTensionBottom.Size = new System.Drawing.Size(120, 56);
            this.TrackBarTensionBottom.TabIndex = 87;
            this.TrackBarTensionBottom.Value = 28;
            // 
            // TrackBarRotateBottom
            // 
            this.TrackBarRotateBottom.Location = new System.Drawing.Point(392, 415);
            this.TrackBarRotateBottom.Maximum = 50;
            this.TrackBarRotateBottom.Minimum = -32;
            this.TrackBarRotateBottom.Name = "TrackBarRotateBottom";
            this.TrackBarRotateBottom.Size = new System.Drawing.Size(120, 56);
            this.TrackBarRotateBottom.TabIndex = 84;
            this.TrackBarRotateBottom.Value = -15;
            // 
            // FernBarnsley
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1445, 703);
            this.Controls.Add(this.TrackBarDistanceBottom);
            this.Controls.Add(this.LabelAcuteBottom);
            this.Controls.Add(this.LabelObtuseBottom);
            this.Controls.Add(this.TrackBarAngleBottom);
            this.Controls.Add(this.LabelWideBottom);
            this.Controls.Add(this.LabelNarrowBottom);
            this.Controls.Add(this.TrackBarWidthBottom);
            this.Controls.Add(this.LabelUnfoldedBottom);
            this.Controls.Add(this.LabelBentBottom);
            this.Controls.Add(this.TrackBarTensionBottom);
            this.Controls.Add(this.TrackBarRotateBottom);
            this.Controls.Add(this.TrackBarDistanceTop);
            this.Controls.Add(this.LabelDistanceTop);
            this.Controls.Add(this.LabelAcuteTop);
            this.Controls.Add(this.LabelObtuseTop);
            this.Controls.Add(this.TrackBarAngleTop);
            this.Controls.Add(this.LabelAngleTop);
            this.Controls.Add(this.LabelWideTop);
            this.Controls.Add(this.LabelNarrowTop);
            this.Controls.Add(this.TrackBarWidthTop);
            this.Controls.Add(this.LabelWidth);
            this.Controls.Add(this.LabelUnfoldedTop);
            this.Controls.Add(this.LabelBentTop);
            this.Controls.Add(this.TrackBarTensionTop);
            this.Controls.Add(this.LabelConvolution);
            this.Controls.Add(this.TrackBarRotateTop);
            this.Controls.Add(this.LabelRotateLeaf);
            this.Controls.Add(this.LabelDownLeaf);
            this.Controls.Add(this.LabelTopLeaf);
            this.Controls.Add(this.LabelBig);
            this.Controls.Add(this.LabelSmall);
            this.Controls.Add(this.LabelSize);
            this.Controls.Add(this.TrackBarSize);
            this.Controls.Add(this.LabelUnfolded);
            this.Controls.Add(this.LabelBent);
            this.Controls.Add(this.LabelTension);
            this.Controls.Add(this.TrackBarTension);
            this.Controls.Add(this.LabelBottom);
            this.Controls.Add(this.LabelTop);
            this.Controls.Add(this.LabelTurn);
            this.Controls.Add(this.TrackBarTurn);
            this.Controls.Add(this.LabelDown2);
            this.Controls.Add(this.LabelUp2);
            this.Controls.Add(this.LabelVertIncline);
            this.Controls.Add(this.TrackBarVertIncline);
            this.Controls.Add(this.LabelRound);
            this.Controls.Add(this.LabelSharp);
            this.Controls.Add(this.LabelLeaf);
            this.Controls.Add(this.TrackBarLeafShape);
            this.Controls.Add(this.LabelDown);
            this.Controls.Add(this.LabelUp);
            this.Controls.Add(this.LabelStemLeaf);
            this.Controls.Add(this.TrackBarStemLeaf);
            this.Controls.Add(this.PictureBoxFern);
            this.Controls.Add(this.ButtonStartOptions);
            this.Controls.Add(this.NumericUpDownIter);
            this.Controls.Add(this.IterationLabel);
            this.Controls.Add(this.ButtonBuild);
            this.Controls.Add(this.LabelAfunc);
            this.Controls.Add(this.ButtonColor);
            this.Controls.Add(this.LabelColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FernBarnsley";
            this.Text = "Папороть Барнслі";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarStemLeaf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarLeafShape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVertIncline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTurn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRotateTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTensionTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarWidthTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAngleTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDistanceTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarDistanceBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarAngleBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarWidthBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarTensionBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarRotateBottom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelColor;
        private System.Windows.Forms.Button ButtonColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label LabelAfunc;
        private System.Windows.Forms.Button ButtonBuild;
        private System.Windows.Forms.Label IterationLabel;
        private System.Windows.Forms.NumericUpDown NumericUpDownIter;
        private System.Windows.Forms.Button ButtonStartOptions;
        private System.Windows.Forms.PictureBox PictureBoxFern;
        private System.Windows.Forms.TrackBar TrackBarStemLeaf;
        private System.Windows.Forms.Label LabelStemLeaf;
        private System.Windows.Forms.Label LabelUp;
        private System.Windows.Forms.Label LabelDown;
        private System.Windows.Forms.Label LabelRound;
        private System.Windows.Forms.Label LabelSharp;
        private System.Windows.Forms.Label LabelLeaf;
        private System.Windows.Forms.TrackBar TrackBarLeafShape;
        private System.Windows.Forms.Label LabelDown2;
        private System.Windows.Forms.Label LabelUp2;
        private System.Windows.Forms.Label LabelVertIncline;
        private System.Windows.Forms.TrackBar TrackBarVertIncline;
        private System.Windows.Forms.Label LabelBottom;
        private System.Windows.Forms.Label LabelTop;
        private System.Windows.Forms.Label LabelTurn;
        private System.Windows.Forms.TrackBar TrackBarTurn;
        private System.Windows.Forms.Label LabelUnfolded;
        private System.Windows.Forms.Label LabelBent;
        private System.Windows.Forms.Label LabelTension;
        private System.Windows.Forms.TrackBar TrackBarTension;
        private System.Windows.Forms.Label LabelBig;
        private System.Windows.Forms.Label LabelSmall;
        private System.Windows.Forms.Label LabelSize;
        private System.Windows.Forms.TrackBar TrackBarSize;
        private System.Windows.Forms.Label LabelTopLeaf;
        private System.Windows.Forms.Label LabelDownLeaf;
        private System.Windows.Forms.Label LabelRotateLeaf;
        private System.Windows.Forms.TrackBar TrackBarRotateTop;
        private System.Windows.Forms.Label LabelConvolution;
        private System.Windows.Forms.Label LabelUnfoldedTop;
        private System.Windows.Forms.Label LabelBentTop;
        private System.Windows.Forms.TrackBar TrackBarTensionTop;
        private System.Windows.Forms.Label LabelWideTop;
        private System.Windows.Forms.Label LabelNarrowTop;
        private System.Windows.Forms.TrackBar TrackBarWidthTop;
        private System.Windows.Forms.Label LabelWidth;
        private System.Windows.Forms.Label LabelAcuteTop;
        private System.Windows.Forms.Label LabelObtuseTop;
        private System.Windows.Forms.TrackBar TrackBarAngleTop;
        private System.Windows.Forms.Label LabelAngleTop;
        private System.Windows.Forms.TrackBar TrackBarDistanceTop;
        private System.Windows.Forms.Label LabelDistanceTop;
        private System.Windows.Forms.TrackBar TrackBarDistanceBottom;
        private System.Windows.Forms.Label LabelAcuteBottom;
        private System.Windows.Forms.Label LabelObtuseBottom;
        private System.Windows.Forms.TrackBar TrackBarAngleBottom;
        private System.Windows.Forms.Label LabelWideBottom;
        private System.Windows.Forms.Label LabelNarrowBottom;
        private System.Windows.Forms.TrackBar TrackBarWidthBottom;
        private System.Windows.Forms.Label LabelUnfoldedBottom;
        private System.Windows.Forms.Label LabelBentBottom;
        private System.Windows.Forms.TrackBar TrackBarTensionBottom;
        private System.Windows.Forms.TrackBar TrackBarRotateBottom;
    }
}