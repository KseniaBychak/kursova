﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FractLib;

namespace Fractals
{
    public partial class DragonCurve : Form
    {
        public DragonCurve()
        {
            InitializeComponent();
        }
        private void ButtonColor_Click(object sender, EventArgs e)
        {
            ColorDialog.AllowFullOpen = false;
            ColorDialog.ShowHelp = true;
            ColorDialog.Color = ButtonColor.BackColor;
            if (ColorDialog.ShowDialog() == DialogResult.OK)
                ButtonColor.BackColor = ColorDialog.Color;
        }
        private void ButtonBuilding_Click(object sender, EventArgs e)
        {
            Bitmap drawArea = new Bitmap(PictureBoxDragon.Width, PictureBoxDragon.Height);
            PictureBoxDragon.Image = drawArea;
            Graphics g = Graphics.FromImage(drawArea);     
            CurveOfDragon dragon=new CurveOfDragon(new Point((int)NumericUpDownX.Value, (int)NumericUpDownY.Value),ButtonColor.BackColor,(int)NumericUpDownIter.Value);
            dragon.Building(g);
        }
    }
}
