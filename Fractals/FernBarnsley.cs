﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FractLib;
using AppendixLibrary;

namespace Fractals
{
    public partial class FernBarnsley : Form
    {
        public FernBarnsley()
        {
            InitializeComponent();
        }
        private void ButtonColor_Click(object sender, EventArgs e)
        {
            colorDialog.AllowFullOpen = false;
            colorDialog.ShowHelp = true;
            colorDialog.Color = ButtonColor.ForeColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
                ButtonColor.BackColor = colorDialog.Color;
        }

        private void ButtonBuild_Click(object sender, EventArgs e)
        {
            Draw();
        }

        private void ButtonStartOptions_Click(object sender, EventArgs e)
        {
            TrackBarStemLeaf.Value = 0;
            TrackBarLeafShape.Value = 85;
            TrackBarVertIncline.Value = 40;
            TrackBarTurn.Value = -4;
            TrackBarTension.Value = 85;
            TrackBarSize.Value = 16;
            TrackBarRotateTop.Value = 20;
            TrackBarTensionTop.Value = -26;
            TrackBarWidthTop.Value = 23;
            TrackBarAngleTop.Value = 22;
            TrackBarDistanceTop.Value = 16;
            TrackBarRotateBottom.Value = -15;
            TrackBarTensionBottom.Value = 28;
            TrackBarWidthBottom.Value = 26;
            TrackBarAngleBottom.Value = 24;
            TrackBarDistanceBottom.Value = 44;
            Draw();
        }
        private void Draw()
        {
            Bitmap drawArea = new Bitmap(PictureBoxFern.Width, PictureBoxFern.Height);
            PictureBoxFern.Image = drawArea;
            Graphics g = Graphics.FromImage(drawArea);
            PointF[] points = new PointF[50];
            points[0] = new PointF(0, 0);
            AfinFunc[] funcs = new AfinFunc[4];
            double[,] matr = new double[2, 2]
            {{TrackBarStemLeaf.Value/10.0,0},
            {0,0.16 } };
            double[] add = new double[2] {0,0};
            funcs[0] = new AfinFunc(matr, add);
            matr = new double[2, 2]
            {{TrackBarLeafShape.Value/100.0,TrackBarVertIncline.Value/1000.0},
            {TrackBarTurn.Value/100.0,TrackBarTension.Value/100.0} };
            add = new double[2] { 0,TrackBarSize.Value/10.0 };
            funcs[1] = new AfinFunc(matr, add);
            matr = new double[2, 2]
            {{TrackBarRotateTop.Value/100.0,TrackBarTensionTop.Value/100.0},
            {TrackBarWidthTop.Value/100.0,TrackBarAngleTop.Value/100.0} };
            add = new double[2] { 0, TrackBarDistanceTop.Value/100.0};
            funcs[2] = new AfinFunc(matr, add);
            matr = new double[2, 2]
            {{TrackBarRotateBottom.Value/100.0,TrackBarTensionBottom.Value/100.0},
            {TrackBarWidthBottom.Value/100.0,TrackBarAngleBottom.Value/100.0} };
            add = new double[2] { 0,TrackBarDistanceBottom.Value/100.0 };
            funcs[3] = new AfinFunc(matr, add);
            Fern fern = new Fern(points, ButtonColor.BackColor, (int)NumericUpDownIter.Value, new PointF(PictureBoxFern.Width / 2, PictureBoxFern.Height / 2), funcs);
            fern.Building(g);
        }
    }
}
