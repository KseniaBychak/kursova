﻿namespace Fractals
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonSierpinskiy = new System.Windows.Forms.Button();
            this.LabelTitle = new System.Windows.Forms.Label();
            this.ButtonDragonCurve = new System.Windows.Forms.Button();
            this.ButtonFern = new System.Windows.Forms.Button();
            this.ButtonKoch = new System.Windows.Forms.Button();
            this.ButtonJulia = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonSierpinskiy
            // 
            this.ButtonSierpinskiy.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSierpinskiy.Location = new System.Drawing.Point(10, 12);
            this.ButtonSierpinskiy.Name = "ButtonSierpinskiy";
            this.ButtonSierpinskiy.Size = new System.Drawing.Size(198, 70);
            this.ButtonSierpinskiy.TabIndex = 0;
            this.ButtonSierpinskiy.Text = "Трикутник Серпинського";
            this.ButtonSierpinskiy.UseVisualStyleBackColor = true;
            this.ButtonSierpinskiy.Click += new System.EventHandler(this.ButtonSierpinskiy_Click);
            // 
            // LabelTitle
            // 
            this.LabelTitle.AutoSize = true;
            this.LabelTitle.BackColor = System.Drawing.Color.Transparent;
            this.LabelTitle.Font = new System.Drawing.Font("Bookman Old Style", 48F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTitle.ForeColor = System.Drawing.Color.Navy;
            this.LabelTitle.Location = new System.Drawing.Point(12, 182);
            this.LabelTitle.Name = "LabelTitle";
            this.LabelTitle.Size = new System.Drawing.Size(511, 93);
            this.LabelTitle.TabIndex = 1;
            this.LabelTitle.Text = "Фрактали";
            // 
            // ButtonDragonCurve
            // 
            this.ButtonDragonCurve.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonDragonCurve.Location = new System.Drawing.Point(219, 12);
            this.ButtonDragonCurve.Name = "ButtonDragonCurve";
            this.ButtonDragonCurve.Size = new System.Drawing.Size(130, 70);
            this.ButtonDragonCurve.TabIndex = 2;
            this.ButtonDragonCurve.Text = "Крива дракона";
            this.ButtonDragonCurve.UseVisualStyleBackColor = true;
            this.ButtonDragonCurve.Click += new System.EventHandler(this.ButtonDragonCurve_Click);
            // 
            // ButtonFern
            // 
            this.ButtonFern.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonFern.Location = new System.Drawing.Point(10, 376);
            this.ButtonFern.Name = "ButtonFern";
            this.ButtonFern.Size = new System.Drawing.Size(261, 70);
            this.ButtonFern.TabIndex = 4;
            this.ButtonFern.Text = "Папороть Барнслі";
            this.ButtonFern.UseVisualStyleBackColor = true;
            this.ButtonFern.Click += new System.EventHandler(this.ButtonFern_Click);
            // 
            // ButtonKoch
            // 
            this.ButtonKoch.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonKoch.Location = new System.Drawing.Point(355, 12);
            this.ButtonKoch.Name = "ButtonKoch";
            this.ButtonKoch.Size = new System.Drawing.Size(189, 70);
            this.ButtonKoch.TabIndex = 5;
            this.ButtonKoch.Text = "Сніжинка Коха";
            this.ButtonKoch.UseVisualStyleBackColor = true;
            this.ButtonKoch.Click += new System.EventHandler(this.ButtonKoch_Click);
            // 
            // ButtonJulia
            // 
            this.ButtonJulia.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonJulia.Location = new System.Drawing.Point(277, 376);
            this.ButtonJulia.Name = "ButtonJulia";
            this.ButtonJulia.Size = new System.Drawing.Size(265, 70);
            this.ButtonJulia.TabIndex = 6;
            this.ButtonJulia.Text = "Множина Жуліа";
            this.ButtonJulia.UseVisualStyleBackColor = true;
            this.ButtonJulia.Click += new System.EventHandler(this.ButtonJulia_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Fractals.Properties.Resources.Квадрат;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(554, 458);
            this.Controls.Add(this.ButtonJulia);
            this.Controls.Add(this.ButtonKoch);
            this.Controls.Add(this.ButtonFern);
            this.Controls.Add(this.ButtonDragonCurve);
            this.Controls.Add(this.LabelTitle);
            this.Controls.Add(this.ButtonSierpinskiy);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Меню";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSierpinskiy;
        private System.Windows.Forms.Label LabelTitle;
        private System.Windows.Forms.Button ButtonDragonCurve;
        private System.Windows.Forms.Button ButtonFern;
        private System.Windows.Forms.Button ButtonKoch;
        private System.Windows.Forms.Button ButtonJulia;
    }
}