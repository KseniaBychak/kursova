﻿namespace Fractals
{
    partial class SnowflakeKoch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel = new System.Windows.Forms.Panel();
            this.CheckBoxAnti = new System.Windows.Forms.CheckBox();
            this.RadioButtonDraw = new System.Windows.Forms.RadioButton();
            this.RadioButtonFill = new System.Windows.Forms.RadioButton();
            this.ButtonColor = new System.Windows.Forms.Button();
            this.ButtonBuild = new System.Windows.Forms.Button();
            this.NumericUpDownIteration = new System.Windows.Forms.NumericUpDown();
            this.LabelInteration = new System.Windows.Forms.Label();
            this.NumericUpDownLength = new System.Windows.Forms.NumericUpDown();
            this.LabelLength = new System.Windows.Forms.Label();
            this.LabelColor = new System.Windows.Forms.Label();
            this.NumericUpDownTop = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDownLeft = new System.Windows.Forms.NumericUpDown();
            this.LabelTop = new System.Windows.Forms.Label();
            this.LabelLeft = new System.Windows.Forms.Label();
            this.LabelMargin = new System.Windows.Forms.Label();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.PictureBoxKoch = new System.Windows.Forms.PictureBox();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIteration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxKoch)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Panel.Controls.Add(this.CheckBoxAnti);
            this.Panel.Controls.Add(this.RadioButtonDraw);
            this.Panel.Controls.Add(this.RadioButtonFill);
            this.Panel.Controls.Add(this.ButtonColor);
            this.Panel.Controls.Add(this.ButtonBuild);
            this.Panel.Controls.Add(this.NumericUpDownIteration);
            this.Panel.Controls.Add(this.LabelInteration);
            this.Panel.Controls.Add(this.NumericUpDownLength);
            this.Panel.Controls.Add(this.LabelLength);
            this.Panel.Controls.Add(this.LabelColor);
            this.Panel.Controls.Add(this.NumericUpDownTop);
            this.Panel.Controls.Add(this.NumericUpDownLeft);
            this.Panel.Controls.Add(this.LabelTop);
            this.Panel.Controls.Add(this.LabelLeft);
            this.Panel.Controls.Add(this.LabelMargin);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(345, 421);
            this.Panel.TabIndex = 0;
            // 
            // CheckBoxAnti
            // 
            this.CheckBoxAnti.AutoSize = true;
            this.CheckBoxAnti.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckBoxAnti.Location = new System.Drawing.Point(18, 284);
            this.CheckBoxAnti.Name = "CheckBoxAnti";
            this.CheckBoxAnti.Size = new System.Drawing.Size(156, 24);
            this.CheckBoxAnti.TabIndex = 1;
            this.CheckBoxAnti.Text = "\"Антисніжинка\"";
            this.CheckBoxAnti.UseVisualStyleBackColor = true;
            // 
            // RadioButtonDraw
            // 
            this.RadioButtonDraw.AutoSize = true;
            this.RadioButtonDraw.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RadioButtonDraw.Location = new System.Drawing.Point(18, 254);
            this.RadioButtonDraw.Name = "RadioButtonDraw";
            this.RadioButtonDraw.Size = new System.Drawing.Size(142, 24);
            this.RadioButtonDraw.TabIndex = 14;
            this.RadioButtonDraw.Text = "Контур фігури";
            this.RadioButtonDraw.UseVisualStyleBackColor = true;
            // 
            // RadioButtonFill
            // 
            this.RadioButtonFill.AutoSize = true;
            this.RadioButtonFill.Checked = true;
            this.RadioButtonFill.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RadioButtonFill.Location = new System.Drawing.Point(18, 224);
            this.RadioButtonFill.Name = "RadioButtonFill";
            this.RadioButtonFill.Size = new System.Drawing.Size(165, 24);
            this.RadioButtonFill.TabIndex = 13;
            this.RadioButtonFill.TabStop = true;
            this.RadioButtonFill.Text = "Заповнена фігура";
            this.RadioButtonFill.UseVisualStyleBackColor = true;
            // 
            // ButtonColor
            // 
            this.ButtonColor.BackColor = System.Drawing.Color.Cyan;
            this.ButtonColor.Location = new System.Drawing.Point(192, 121);
            this.ButtonColor.Name = "ButtonColor";
            this.ButtonColor.Size = new System.Drawing.Size(33, 25);
            this.ButtonColor.TabIndex = 12;
            this.ButtonColor.UseVisualStyleBackColor = false;
            this.ButtonColor.Click += new System.EventHandler(this.ButtonColor_Click);
            // 
            // ButtonBuild
            // 
            this.ButtonBuild.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonBuild.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBuild.Location = new System.Drawing.Point(13, 358);
            this.ButtonBuild.Name = "ButtonBuild";
            this.ButtonBuild.Size = new System.Drawing.Size(303, 46);
            this.ButtonBuild.TabIndex = 11;
            this.ButtonBuild.Text = "Побудувати ";
            this.ButtonBuild.UseVisualStyleBackColor = true;
            this.ButtonBuild.Click += new System.EventHandler(this.ButtonBuild_Click);
            // 
            // NumericUpDownIteration
            // 
            this.NumericUpDownIteration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownIteration.Location = new System.Drawing.Point(222, 184);
            this.NumericUpDownIteration.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumericUpDownIteration.Name = "NumericUpDownIteration";
            this.NumericUpDownIteration.Size = new System.Drawing.Size(120, 27);
            this.NumericUpDownIteration.TabIndex = 10;
            // 
            // LabelInteration
            // 
            this.LabelInteration.AutoSize = true;
            this.LabelInteration.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelInteration.Location = new System.Drawing.Point(13, 184);
            this.LabelInteration.Name = "LabelInteration";
            this.LabelInteration.Size = new System.Drawing.Size(185, 26);
            this.LabelInteration.TabIndex = 9;
            this.LabelInteration.Text = "Кількість ітерацій";
            // 
            // NumericUpDownLength
            // 
            this.NumericUpDownLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownLength.Location = new System.Drawing.Point(222, 150);
            this.NumericUpDownLength.Maximum = new decimal(new int[] {
            650,
            0,
            0,
            0});
            this.NumericUpDownLength.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NumericUpDownLength.Name = "NumericUpDownLength";
            this.NumericUpDownLength.Size = new System.Drawing.Size(120, 27);
            this.NumericUpDownLength.TabIndex = 8;
            this.NumericUpDownLength.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // LabelLength
            // 
            this.LabelLength.AutoSize = true;
            this.LabelLength.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelLength.Location = new System.Drawing.Point(11, 147);
            this.LabelLength.Name = "LabelLength";
            this.LabelLength.Size = new System.Drawing.Size(186, 26);
            this.LabelLength.TabIndex = 7;
            this.LabelLength.Text = "Довжина сторони";
            // 
            // LabelColor
            // 
            this.LabelColor.AutoSize = true;
            this.LabelColor.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelColor.Location = new System.Drawing.Point(8, 118);
            this.LabelColor.Name = "LabelColor";
            this.LabelColor.Size = new System.Drawing.Size(163, 26);
            this.LabelColor.TabIndex = 5;
            this.LabelColor.Text = "Колір фракталу";
            // 
            // NumericUpDownTop
            // 
            this.NumericUpDownTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownTop.Location = new System.Drawing.Point(105, 72);
            this.NumericUpDownTop.Maximum = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.NumericUpDownTop.Name = "NumericUpDownTop";
            this.NumericUpDownTop.Size = new System.Drawing.Size(120, 27);
            this.NumericUpDownTop.TabIndex = 4;
            this.NumericUpDownTop.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // NumericUpDownLeft
            // 
            this.NumericUpDownLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownLeft.Location = new System.Drawing.Point(105, 39);
            this.NumericUpDownLeft.Maximum = new decimal(new int[] {
            1140,
            0,
            0,
            0});
            this.NumericUpDownLeft.Name = "NumericUpDownLeft";
            this.NumericUpDownLeft.Size = new System.Drawing.Size(120, 27);
            this.NumericUpDownLeft.TabIndex = 3;
            this.NumericUpDownLeft.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // LabelTop
            // 
            this.LabelTop.AutoSize = true;
            this.LabelTop.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTop.Location = new System.Drawing.Point(10, 73);
            this.LabelTop.Name = "LabelTop";
            this.LabelTop.Size = new System.Drawing.Size(84, 26);
            this.LabelTop.TabIndex = 2;
            this.LabelTop.Text = "Зверху:";
            // 
            // LabelLeft
            // 
            this.LabelLeft.AutoSize = true;
            this.LabelLeft.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelLeft.Location = new System.Drawing.Point(8, 40);
            this.LabelLeft.Name = "LabelLeft";
            this.LabelLeft.Size = new System.Drawing.Size(68, 26);
            this.LabelLeft.TabIndex = 1;
            this.LabelLeft.Text = "Зліва:";
            // 
            // LabelMargin
            // 
            this.LabelMargin.AutoSize = true;
            this.LabelMargin.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelMargin.Location = new System.Drawing.Point(65, 10);
            this.LabelMargin.Name = "LabelMargin";
            this.LabelMargin.Size = new System.Drawing.Size(176, 26);
            this.LabelMargin.TabIndex = 0;
            this.LabelMargin.Text = "Відступи фігури:";
            // 
            // PictureBoxKoch
            // 
            this.PictureBoxKoch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxKoch.Location = new System.Drawing.Point(363, 12);
            this.PictureBoxKoch.Name = "PictureBoxKoch";
            this.PictureBoxKoch.Size = new System.Drawing.Size(439, 421);
            this.PictureBoxKoch.TabIndex = 1;
            this.PictureBoxKoch.TabStop = false;
            // 
            // SnowflakeKoch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 445);
            this.Controls.Add(this.PictureBoxKoch);
            this.Controls.Add(this.Panel);
            this.Name = "SnowflakeKoch";
            this.Text = "Сніжинка Коха";
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIteration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxKoch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Label LabelMargin;
        private System.Windows.Forms.NumericUpDown NumericUpDownLength;
        private System.Windows.Forms.Label LabelLength;
        private System.Windows.Forms.Label LabelColor;
        private System.Windows.Forms.NumericUpDown NumericUpDownTop;
        private System.Windows.Forms.NumericUpDown NumericUpDownLeft;
        private System.Windows.Forms.Label LabelTop;
        private System.Windows.Forms.Label LabelLeft;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Button ButtonBuild;
        private System.Windows.Forms.NumericUpDown NumericUpDownIteration;
        private System.Windows.Forms.Label LabelInteration;
        private System.Windows.Forms.Button ButtonColor;
        private System.Windows.Forms.CheckBox CheckBoxAnti;
        private System.Windows.Forms.RadioButton RadioButtonDraw;
        private System.Windows.Forms.RadioButton RadioButtonFill;
        private System.Windows.Forms.PictureBox PictureBoxKoch;
    }
}