﻿namespace Fractals
{
    partial class Julia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureBoxSet = new System.Windows.Forms.PictureBox();
            this.ButtonColorScheme = new System.Windows.Forms.Button();
            this.LabelColorScheme = new System.Windows.Forms.Label();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.LabelC = new System.Windows.Forms.Label();
            this.NumericUpDownReal = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDownIm = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownReal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIm)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBoxSet
            // 
            this.PictureBoxSet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PictureBoxSet.Location = new System.Drawing.Point(281, 3);
            this.PictureBoxSet.Name = "PictureBoxSet";
            this.PictureBoxSet.Size = new System.Drawing.Size(300, 300);
            this.PictureBoxSet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxSet.TabIndex = 18;
            this.PictureBoxSet.TabStop = false;
            // 
            // ButtonColorScheme
            // 
            this.ButtonColorScheme.BackColor = System.Drawing.Color.Red;
            this.ButtonColorScheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonColorScheme.Location = new System.Drawing.Point(246, 22);
            this.ButtonColorScheme.Name = "ButtonColorScheme";
            this.ButtonColorScheme.Size = new System.Drawing.Size(28, 23);
            this.ButtonColorScheme.TabIndex = 20;
            this.ButtonColorScheme.UseVisualStyleBackColor = false;
            this.ButtonColorScheme.Click += new System.EventHandler(this.ButtonColorScheme_Click);
            // 
            // LabelColorScheme
            // 
            this.LabelColorScheme.AutoSize = true;
            this.LabelColorScheme.Font = new System.Drawing.Font("Bookman Old Style", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelColorScheme.Location = new System.Drawing.Point(12, 22);
            this.LabelColorScheme.Name = "LabelColorScheme";
            this.LabelColorScheme.Size = new System.Drawing.Size(211, 24);
            this.LabelColorScheme.TabIndex = 19;
            this.LabelColorScheme.Text = "Кольорова гамма:";
            // 
            // LabelC
            // 
            this.LabelC.AutoSize = true;
            this.LabelC.Font = new System.Drawing.Font("Bookman Old Style", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelC.Location = new System.Drawing.Point(15, 65);
            this.LabelC.Name = "LabelC";
            this.LabelC.Size = new System.Drawing.Size(259, 24);
            this.LabelC.TabIndex = 22;
            this.LabelC.Text = "C=              +               i";
            // 
            // NumericUpDownReal
            // 
            this.NumericUpDownReal.DecimalPlaces = 4;
            this.NumericUpDownReal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownReal.Increment = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            this.NumericUpDownReal.Location = new System.Drawing.Point(58, 61);
            this.NumericUpDownReal.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NumericUpDownReal.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            this.NumericUpDownReal.Name = "NumericUpDownReal";
            this.NumericUpDownReal.Size = new System.Drawing.Size(82, 28);
            this.NumericUpDownReal.TabIndex = 23;
            this.NumericUpDownReal.ValueChanged += new System.EventHandler(this.NumBoxReal_ValueChanged);
            // 
            // NumericUpDownIm
            // 
            this.NumericUpDownIm.DecimalPlaces = 4;
            this.NumericUpDownIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownIm.Increment = new decimal(new int[] {
            25,
            0,
            0,
            196608});
            this.NumericUpDownIm.Location = new System.Drawing.Point(167, 61);
            this.NumericUpDownIm.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NumericUpDownIm.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            this.NumericUpDownIm.Name = "NumericUpDownIm";
            this.NumericUpDownIm.Size = new System.Drawing.Size(82, 28);
            this.NumericUpDownIm.TabIndex = 24;
            this.NumericUpDownIm.ValueChanged += new System.EventHandler(this.NumBoxIm_ValueChanged);
            // 
            // Julia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 310);
            this.Controls.Add(this.NumericUpDownIm);
            this.Controls.Add(this.NumericUpDownReal);
            this.Controls.Add(this.LabelC);
            this.Controls.Add(this.PictureBoxSet);
            this.Controls.Add(this.ButtonColorScheme);
            this.Controls.Add(this.LabelColorScheme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Julia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Множина Жуліа";
            this.Load += new System.EventHandler(this.Julia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownReal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox PictureBoxSet;
        private System.Windows.Forms.Button ButtonColorScheme;
        private System.Windows.Forms.Label LabelColorScheme;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Label LabelC;
        private System.Windows.Forms.NumericUpDown NumericUpDownReal;
        private System.Windows.Forms.NumericUpDown NumericUpDownIm;
    }
}