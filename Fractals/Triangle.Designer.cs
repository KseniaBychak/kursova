﻿namespace Fractals
{
    partial class Triangle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelCoordinat = new System.Windows.Forms.Label();
            this.NumericUpDown1X = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown1Y = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown2Y = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown2X = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown3Y = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDown3X = new System.Windows.Forms.NumericUpDown();
            this.LabelX = new System.Windows.Forms.Label();
            this.LabelY = new System.Windows.Forms.Label();
            this.LabelIteration = new System.Windows.Forms.Label();
            this.NumericUpDownIter = new System.Windows.Forms.NumericUpDown();
            this.LabelColor = new System.Windows.Forms.Label();
            this.ButtonColor = new System.Windows.Forms.Button();
            this.ButtonBuilding = new System.Windows.Forms.Button();
            this.NumericUpDownWOfPoint = new System.Windows.Forms.NumericUpDown();
            this.LabelWidthofPoint = new System.Windows.Forms.Label();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.PictureBoxTriangle = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown1X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown1Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown2Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown2X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown3Y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown3X)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownWOfPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTriangle)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelCoordinat
            // 
            this.LabelCoordinat.AutoSize = true;
            this.LabelCoordinat.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCoordinat.Location = new System.Drawing.Point(91, 12);
            this.LabelCoordinat.Name = "LabelCoordinat";
            this.LabelCoordinat.Size = new System.Drawing.Size(313, 26);
            this.LabelCoordinat.TabIndex = 1;
            this.LabelCoordinat.Text = "Координати початкових точок:";
            // 
            // NumericUpDown1X
            // 
            this.NumericUpDown1X.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown1X.Location = new System.Drawing.Point(162, 66);
            this.NumericUpDown1X.Maximum = new decimal(new int[] {
            1120,
            0,
            0,
            0});
            this.NumericUpDown1X.Name = "NumericUpDown1X";
            this.NumericUpDown1X.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown1X.TabIndex = 2;
            this.NumericUpDown1X.Value = new decimal(new int[] {
            135,
            0,
            0,
            0});
            // 
            // NumericUpDown1Y
            // 
            this.NumericUpDown1Y.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown1Y.Location = new System.Drawing.Point(254, 66);
            this.NumericUpDown1Y.Maximum = new decimal(new int[] {
            760,
            0,
            0,
            0});
            this.NumericUpDown1Y.Name = "NumericUpDown1Y";
            this.NumericUpDown1Y.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown1Y.TabIndex = 3;
            // 
            // NumericUpDown2Y
            // 
            this.NumericUpDown2Y.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown2Y.Location = new System.Drawing.Point(254, 100);
            this.NumericUpDown2Y.Maximum = new decimal(new int[] {
            760,
            0,
            0,
            0});
            this.NumericUpDown2Y.Name = "NumericUpDown2Y";
            this.NumericUpDown2Y.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown2Y.TabIndex = 5;
            this.NumericUpDown2Y.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            // 
            // NumericUpDown2X
            // 
            this.NumericUpDown2X.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown2X.Location = new System.Drawing.Point(162, 100);
            this.NumericUpDown2X.Maximum = new decimal(new int[] {
            1120,
            0,
            0,
            0});
            this.NumericUpDown2X.Name = "NumericUpDown2X";
            this.NumericUpDown2X.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown2X.TabIndex = 4;
            // 
            // NumericUpDown3Y
            // 
            this.NumericUpDown3Y.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown3Y.Location = new System.Drawing.Point(254, 134);
            this.NumericUpDown3Y.Maximum = new decimal(new int[] {
            760,
            0,
            0,
            0});
            this.NumericUpDown3Y.Name = "NumericUpDown3Y";
            this.NumericUpDown3Y.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown3Y.TabIndex = 7;
            this.NumericUpDown3Y.Value = new decimal(new int[] {
            270,
            0,
            0,
            0});
            // 
            // NumericUpDown3X
            // 
            this.NumericUpDown3X.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDown3X.Location = new System.Drawing.Point(162, 134);
            this.NumericUpDown3X.Maximum = new decimal(new int[] {
            1120,
            0,
            0,
            0});
            this.NumericUpDown3X.Name = "NumericUpDown3X";
            this.NumericUpDown3X.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDown3X.TabIndex = 6;
            this.NumericUpDown3X.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            // 
            // LabelX
            // 
            this.LabelX.AutoSize = true;
            this.LabelX.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelX.Location = new System.Drawing.Point(181, 38);
            this.LabelX.Name = "LabelX";
            this.LabelX.Size = new System.Drawing.Size(27, 25);
            this.LabelX.TabIndex = 8;
            this.LabelX.Text = "X";
            // 
            // LabelY
            // 
            this.LabelY.AutoSize = true;
            this.LabelY.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelY.Location = new System.Drawing.Point(273, 38);
            this.LabelY.Name = "LabelY";
            this.LabelY.Size = new System.Drawing.Size(27, 25);
            this.LabelY.TabIndex = 9;
            this.LabelY.Text = "Y";
            // 
            // LabelIteration
            // 
            this.LabelIteration.AutoSize = true;
            this.LabelIteration.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelIteration.Location = new System.Drawing.Point(9, 178);
            this.LabelIteration.Name = "LabelIteration";
            this.LabelIteration.Size = new System.Drawing.Size(191, 26);
            this.LabelIteration.TabIndex = 10;
            this.LabelIteration.Text = "Кількість ітерацій:";
            // 
            // NumericUpDownIter
            // 
            this.NumericUpDownIter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownIter.Increment = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.NumericUpDownIter.Location = new System.Drawing.Point(219, 178);
            this.NumericUpDownIter.Maximum = new decimal(new int[] {
            400000,
            0,
            0,
            0});
            this.NumericUpDownIter.Name = "NumericUpDownIter";
            this.NumericUpDownIter.Size = new System.Drawing.Size(149, 28);
            this.NumericUpDownIter.TabIndex = 11;
            // 
            // LabelColor
            // 
            this.LabelColor.AutoSize = true;
            this.LabelColor.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelColor.Location = new System.Drawing.Point(9, 216);
            this.LabelColor.Name = "LabelColor";
            this.LabelColor.Size = new System.Drawing.Size(135, 26);
            this.LabelColor.TabIndex = 12;
            this.LabelColor.Text = "Колір точок:";
            // 
            // ButtonColor
            // 
            this.ButtonColor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ButtonColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonColor.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonColor.ForeColor = System.Drawing.Color.Yellow;
            this.ButtonColor.Location = new System.Drawing.Point(150, 212);
            this.ButtonColor.Name = "ButtonColor";
            this.ButtonColor.Size = new System.Drawing.Size(58, 37);
            this.ButtonColor.TabIndex = 14;
            this.ButtonColor.UseVisualStyleBackColor = false;
            this.ButtonColor.Click += new System.EventHandler(this.ButtonColor_Click);
            // 
            // ButtonBuilding
            // 
            this.ButtonBuilding.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.ButtonBuilding.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBuilding.Font = new System.Drawing.Font("Bookman Old Style", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBuilding.ForeColor = System.Drawing.Color.Yellow;
            this.ButtonBuilding.Location = new System.Drawing.Point(14, 295);
            this.ButtonBuilding.Name = "ButtonBuilding";
            this.ButtonBuilding.Size = new System.Drawing.Size(476, 49);
            this.ButtonBuilding.TabIndex = 18;
            this.ButtonBuilding.Text = "Побудувати!";
            this.ButtonBuilding.UseVisualStyleBackColor = false;
            this.ButtonBuilding.Click += new System.EventHandler(this.ButtonBuilding_Click);
            // 
            // NumericUpDownWOfPoint
            // 
            this.NumericUpDownWOfPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownWOfPoint.Location = new System.Drawing.Point(332, 253);
            this.NumericUpDownWOfPoint.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.NumericUpDownWOfPoint.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumericUpDownWOfPoint.Name = "NumericUpDownWOfPoint";
            this.NumericUpDownWOfPoint.Size = new System.Drawing.Size(72, 28);
            this.NumericUpDownWOfPoint.TabIndex = 20;
            this.NumericUpDownWOfPoint.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LabelWidthofPoint
            // 
            this.LabelWidthofPoint.AutoSize = true;
            this.LabelWidthofPoint.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelWidthofPoint.Location = new System.Drawing.Point(9, 253);
            this.LabelWidthofPoint.Name = "LabelWidthofPoint";
            this.LabelWidthofPoint.Size = new System.Drawing.Size(279, 26);
            this.LabelWidthofPoint.TabIndex = 19;
            this.LabelWidthofPoint.Text = "Ширина точок (в пікселях):";
            // 
            // PictureBoxTriangle
            // 
            this.PictureBoxTriangle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxTriangle.Location = new System.Drawing.Point(505, 12);
            this.PictureBoxTriangle.Name = "PictureBoxTriangle";
            this.PictureBoxTriangle.Size = new System.Drawing.Size(405, 412);
            this.PictureBoxTriangle.TabIndex = 20;
            this.PictureBoxTriangle.TabStop = false;
            // 
            // Triangle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Yellow;
            this.ClientSize = new System.Drawing.Size(922, 436);
            this.Controls.Add(this.NumericUpDownWOfPoint);
            this.Controls.Add(this.PictureBoxTriangle);
            this.Controls.Add(this.LabelWidthofPoint);
            this.Controls.Add(this.LabelCoordinat);
            this.Controls.Add(this.ButtonBuilding);
            this.Controls.Add(this.LabelX);
            this.Controls.Add(this.NumericUpDown1X);
            this.Controls.Add(this.LabelY);
            this.Controls.Add(this.ButtonColor);
            this.Controls.Add(this.NumericUpDown3Y);
            this.Controls.Add(this.NumericUpDown1Y);
            this.Controls.Add(this.LabelIteration);
            this.Controls.Add(this.NumericUpDown2X);
            this.Controls.Add(this.NumericUpDown3X);
            this.Controls.Add(this.LabelColor);
            this.Controls.Add(this.NumericUpDownIter);
            this.Controls.Add(this.NumericUpDown2Y);
            this.MinimumSize = new System.Drawing.Size(905, 430);
            this.Name = "Triangle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Трикутник Серпінського";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown1X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown1Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown2Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown2X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown3Y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDown3X)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownWOfPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxTriangle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LabelCoordinat;
        private System.Windows.Forms.NumericUpDown NumericUpDown1X;
        private System.Windows.Forms.NumericUpDown NumericUpDown1Y;
        private System.Windows.Forms.NumericUpDown NumericUpDown2Y;
        private System.Windows.Forms.NumericUpDown NumericUpDown2X;
        private System.Windows.Forms.NumericUpDown NumericUpDown3Y;
        private System.Windows.Forms.NumericUpDown NumericUpDown3X;
        private System.Windows.Forms.Label LabelX;
        private System.Windows.Forms.Label LabelY;
        private System.Windows.Forms.Label LabelIteration;
        private System.Windows.Forms.NumericUpDown NumericUpDownIter;
        private System.Windows.Forms.Label LabelColor;
        private System.Windows.Forms.Button ButtonColor;
        private System.Windows.Forms.Button ButtonBuilding;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.NumericUpDown NumericUpDownWOfPoint;
        private System.Windows.Forms.Label LabelWidthofPoint;
        private System.Windows.Forms.PictureBox PictureBoxTriangle;
    }
}