﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FractLib;

namespace Fractals
{
    public partial class Triangle : Form
    {

        public Triangle()
        {
            InitializeComponent();
        }
        private void ButtonBuilding_Click(object sender, EventArgs e)
        {
            Bitmap drawArea = new Bitmap(PictureBoxTriangle.Width, PictureBoxTriangle.Height);
            PictureBoxTriangle.Image = drawArea;
            Graphics g = Graphics.FromImage(drawArea);
            SierpinskiyTriangle triangle;
            PointF[] points = new PointF[3 + (long)NumericUpDownIter.Value];
            points[0] = new PointF((float)NumericUpDown1X.Value, (float)NumericUpDown1Y.Value);
            points[1] = new PointF((float)NumericUpDown2X.Value, (float)NumericUpDown2Y.Value);
            points[2] = new PointF((float)NumericUpDown3X.Value, (float)NumericUpDown3Y.Value);
                g.Clear(BackColor);
                triangle = new SierpinskiyTriangle(points,ButtonColor.BackColor,(int)NumericUpDownWOfPoint.Value);
                triangle.Building(g);
        }
        private void ButtonColor_Click(object sender, EventArgs e)
        {
            ColorDialog.AllowFullOpen = false;
            ColorDialog.ShowHelp = true;
            ColorDialog.Color = ButtonColor.BackColor;
            if (ColorDialog.ShowDialog() == DialogResult.OK)
                ButtonColor.BackColor = ColorDialog.Color;
        }
    }
}
