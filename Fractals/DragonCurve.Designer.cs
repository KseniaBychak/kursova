﻿namespace Fractals
{
    partial class DragonCurve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.LabelColor = new System.Windows.Forms.Label();
            this.NumericUpDownY = new System.Windows.Forms.NumericUpDown();
            this.LabelY = new System.Windows.Forms.Label();
            this.ButtonColor = new System.Windows.Forms.Button();
            this.NumericUpDownX = new System.Windows.Forms.NumericUpDown();
            this.LabelIteration = new System.Windows.Forms.Label();
            this.LabelX = new System.Windows.Forms.Label();
            this.NumericUpDownIter = new System.Windows.Forms.NumericUpDown();
            this.LabelStartPoint = new System.Windows.Forms.Label();
            this.ButtonBuilding = new System.Windows.Forms.Button();
            this.PictureBoxDragon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDragon)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelColor
            // 
            this.LabelColor.AutoSize = true;
            this.LabelColor.BackColor = System.Drawing.Color.Transparent;
            this.LabelColor.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelColor.ForeColor = System.Drawing.Color.Yellow;
            this.LabelColor.Location = new System.Drawing.Point(20, 106);
            this.LabelColor.Name = "LabelColor";
            this.LabelColor.Size = new System.Drawing.Size(169, 26);
            this.LabelColor.TabIndex = 5;
            this.LabelColor.Text = "Колір фракталу:";
            // 
            // NumericUpDownY
            // 
            this.NumericUpDownY.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownY.Location = new System.Drawing.Point(60, 76);
            this.NumericUpDownY.Maximum = new decimal(new int[] {
            420,
            0,
            0,
            0});
            this.NumericUpDownY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NumericUpDownY.Name = "NumericUpDownY";
            this.NumericUpDownY.Size = new System.Drawing.Size(71, 27);
            this.NumericUpDownY.TabIndex = 4;
            this.NumericUpDownY.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // LabelY
            // 
            this.LabelY.AutoSize = true;
            this.LabelY.BackColor = System.Drawing.Color.Transparent;
            this.LabelY.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelY.ForeColor = System.Drawing.Color.Yellow;
            this.LabelY.Location = new System.Drawing.Point(21, 76);
            this.LabelY.Name = "LabelY";
            this.LabelY.Size = new System.Drawing.Size(33, 25);
            this.LabelY.TabIndex = 3;
            this.LabelY.Text = "Y:";
            // 
            // ButtonColor
            // 
            this.ButtonColor.BackColor = System.Drawing.Color.Gold;
            this.ButtonColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonColor.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonColor.ForeColor = System.Drawing.Color.Gold;
            this.ButtonColor.Location = new System.Drawing.Point(207, 105);
            this.ButtonColor.Name = "ButtonColor";
            this.ButtonColor.Size = new System.Drawing.Size(39, 31);
            this.ButtonColor.TabIndex = 7;
            this.ButtonColor.UseVisualStyleBackColor = false;
            this.ButtonColor.Click += new System.EventHandler(this.ButtonColor_Click);
            // 
            // NumericUpDownX
            // 
            this.NumericUpDownX.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownX.Location = new System.Drawing.Point(61, 40);
            this.NumericUpDownX.Maximum = new decimal(new int[] {
            1100,
            0,
            0,
            0});
            this.NumericUpDownX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NumericUpDownX.Name = "NumericUpDownX";
            this.NumericUpDownX.Size = new System.Drawing.Size(70, 27);
            this.NumericUpDownX.TabIndex = 2;
            this.NumericUpDownX.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // LabelIteration
            // 
            this.LabelIteration.AutoSize = true;
            this.LabelIteration.BackColor = System.Drawing.Color.Transparent;
            this.LabelIteration.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelIteration.ForeColor = System.Drawing.Color.Yellow;
            this.LabelIteration.Location = new System.Drawing.Point(20, 155);
            this.LabelIteration.Name = "LabelIteration";
            this.LabelIteration.Size = new System.Drawing.Size(191, 26);
            this.LabelIteration.TabIndex = 8;
            this.LabelIteration.Text = "Кількість ітерацій:";
            // 
            // LabelX
            // 
            this.LabelX.AutoSize = true;
            this.LabelX.BackColor = System.Drawing.Color.Transparent;
            this.LabelX.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelX.ForeColor = System.Drawing.Color.Yellow;
            this.LabelX.Location = new System.Drawing.Point(20, 40);
            this.LabelX.Name = "LabelX";
            this.LabelX.Size = new System.Drawing.Size(35, 25);
            this.LabelX.TabIndex = 1;
            this.LabelX.Text = "X:";
            // 
            // NumericUpDownIter
            // 
            this.NumericUpDownIter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumericUpDownIter.Location = new System.Drawing.Point(217, 155);
            this.NumericUpDownIter.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.NumericUpDownIter.Name = "NumericUpDownIter";
            this.NumericUpDownIter.Size = new System.Drawing.Size(59, 27);
            this.NumericUpDownIter.TabIndex = 9;
            // 
            // LabelStartPoint
            // 
            this.LabelStartPoint.AutoSize = true;
            this.LabelStartPoint.BackColor = System.Drawing.Color.Transparent;
            this.LabelStartPoint.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelStartPoint.ForeColor = System.Drawing.Color.Yellow;
            this.LabelStartPoint.Location = new System.Drawing.Point(20, 14);
            this.LabelStartPoint.Name = "LabelStartPoint";
            this.LabelStartPoint.Size = new System.Drawing.Size(178, 26);
            this.LabelStartPoint.TabIndex = 0;
            this.LabelStartPoint.Text = "Початкова точка:";
            // 
            // ButtonBuilding
            // 
            this.ButtonBuilding.BackColor = System.Drawing.Color.Yellow;
            this.ButtonBuilding.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBuilding.Font = new System.Drawing.Font("Times New Roman", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonBuilding.ForeColor = System.Drawing.Color.Red;
            this.ButtonBuilding.Location = new System.Drawing.Point(25, 203);
            this.ButtonBuilding.Name = "ButtonBuilding";
            this.ButtonBuilding.Size = new System.Drawing.Size(251, 36);
            this.ButtonBuilding.TabIndex = 10;
            this.ButtonBuilding.Text = "Побудувати!";
            this.ButtonBuilding.UseVisualStyleBackColor = false;
            this.ButtonBuilding.Click += new System.EventHandler(this.ButtonBuilding_Click);
            // 
            // PictureBoxDragon
            // 
            this.PictureBoxDragon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxDragon.Location = new System.Drawing.Point(282, 12);
            this.PictureBoxDragon.Name = "PictureBoxDragon";
            this.PictureBoxDragon.Size = new System.Drawing.Size(548, 389);
            this.PictureBoxDragon.TabIndex = 11;
            this.PictureBoxDragon.TabStop = false;
            // 
            // DragonCurve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(842, 413);
            this.Controls.Add(this.PictureBoxDragon);
            this.Controls.Add(this.ButtonBuilding);
            this.Controls.Add(this.LabelStartPoint);
            this.Controls.Add(this.NumericUpDownIter);
            this.Controls.Add(this.LabelColor);
            this.Controls.Add(this.LabelX);
            this.Controls.Add(this.NumericUpDownY);
            this.Controls.Add(this.LabelIteration);
            this.Controls.Add(this.NumericUpDownX);
            this.Controls.Add(this.LabelY);
            this.Controls.Add(this.ButtonColor);
            this.ForeColor = System.Drawing.Color.Yellow;
            this.MinimumSize = new System.Drawing.Size(500, 460);
            this.Name = "DragonCurve";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Крива дракона";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownIter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDragon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Label LabelColor;
        private System.Windows.Forms.NumericUpDown NumericUpDownY;
        private System.Windows.Forms.Label LabelY;
        private System.Windows.Forms.Button ButtonColor;
        private System.Windows.Forms.NumericUpDown NumericUpDownX;
        private System.Windows.Forms.Label LabelIteration;
        private System.Windows.Forms.Label LabelX;
        private System.Windows.Forms.NumericUpDown NumericUpDownIter;
        private System.Windows.Forms.Label LabelStartPoint;
        private System.Windows.Forms.Button ButtonBuilding;
        private System.Windows.Forms.PictureBox PictureBoxDragon;
    }
}