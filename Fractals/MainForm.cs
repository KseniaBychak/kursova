﻿using System;
using System.Windows.Forms;

namespace Fractals
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ButtonSierpinskiy_Click(object sender, EventArgs e)
        {
            Triangle f = new Triangle();
            f.Show();
        }

        private void ButtonDragonCurve_Click(object sender, EventArgs e)
        {
            DragonCurve f = new DragonCurve();
            f.Show();
        }
        private void ButtonFern_Click(object sender, EventArgs e)
        {
            FernBarnsley f = new FernBarnsley();
            f.Show();
        }

        private void ButtonKoch_Click(object sender, EventArgs e)
        {
            SnowflakeKoch f = new SnowflakeKoch();
            f.Show();
        }

        private void ButtonJulia_Click(object sender, EventArgs e)
        {
            Julia f=new Julia();
            f.Show();
        }
    }
}
