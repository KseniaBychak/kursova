﻿using System;
using System.Drawing;

namespace FractLib
{
    public class SierpinskiyTriangle : Fractal
    {
        public PointF[] Points { get; set; }
        public override Color ColorOfPoint { get; set; }
        public int WidthOfPoint { get; set; }
        public SierpinskiyTriangle(PointF[] points,Color color,int widthofpoint)
        {
            Points = points;
            ColorOfPoint = color;
            WidthOfPoint = widthofpoint;
        }
        public override void Building(Graphics g)
        {
            Random rand = new Random();
            float x, y;
            int p;
            for (int i=0; i < Points.LongLength; i++)
            {
                if (i == 3) Points[i] = new PointF((Points[0].X + Points[1].X) / 2, (Points[0].Y + Points[1].Y) / 2);
                if (i > 3)
                {
                    p = rand.Next(0, 3);
                    x = (Points[i - 1].X + Points[p].X) / 2;
                    y = (Points[i - 1].Y + Points[p].Y) / 2;
                    Points[i] = new PointF(x, y);
                }
                g.FillRectangle(new SolidBrush(ColorOfPoint), Points[i].X, Points[i].Y, WidthOfPoint, WidthOfPoint);
            }
        }
    }
}
