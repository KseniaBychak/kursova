﻿using System.Drawing;
namespace FractLib
{
    public abstract class Fractal
    {
        public abstract Color ColorOfPoint { get; set; }
        public abstract void Building(Graphics g);
    }
}
