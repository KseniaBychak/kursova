﻿using System.Drawing;

namespace FractLib
{
    public class CurveOfDragon : Fractal
    {
        public Point StartPoint { get; set; }
        public override Color ColorOfPoint { get; set; }
        public int Iteration { get; set; }
        private string Turns;
        public CurveOfDragon(Point point,Color color,int iteration)
        {
            StartPoint = point;
            ColorOfPoint = color;
            Iteration = iteration;
        }
        public override void Building(Graphics g)
        {
            Turns = "R";
            AddTurns();
            int x=StartPoint.X;
            int y=StartPoint.Y;
            int indexX = 0;
            int indexY = 0;
            int count = 0;
            for (int j = 0; j < Turns.Length; j++)
            {
                if (Turns[j] == 'R')
                {
                    count++;
                    if (count == 5) count = 1;
                }
                else if (Turns[j] == 'L')
                {
                    count--;
                    if (count == 0) count = 4;
                }
                if (count == 1) indexY -= 2;
                else if (count == 2) indexX += 2;
                else if (count == 3) indexY += 2;
                else if (count == 4) indexX -= 2;
                g.DrawLine(new Pen(ColorOfPoint), x, y, x + indexX, y + indexY);
                x = x + indexX;
                y = y + indexY;
                indexX = 0;
                indexY = 0;
            }
        }
        public void AddTurns()
        {
            for (int i = 1; i <Iteration; i++)
            {
                if (i > 0)
                {
                    string turns2 = "";
                    Turns += 'R';
                    for (int r = Turns.Length - 2; r >= 0; r--)
                    {
                        if (Turns[r] == 'L') turns2 += 'R';
                        else turns2 += 'L';
                    }
                    Turns += turns2;
                }
            }
        }
    }
}
