﻿using System;
using System.Drawing;
using AppendixLibrary;

namespace FractLib
{
    public class Fern : Fractal
    {
        public PointF[] Points { get; set; }
        public override Color ColorOfPoint { get; set; }
        public int Iteration { get; set; }
        public PointF Center { get; set; }
        public AfinFunc [] Funcs { get; set; }
        public Fern(PointF[] points,Color color, int iteration,PointF center,AfinFunc[] funcs)
        {
            Points=points;
            ColorOfPoint = color;
            Iteration = iteration;
            Center = center;
            Funcs=funcs;
        }

        public override void Building(Graphics g)
        {
            Random rand= new Random();
            int f;
            int k = 0;
            for (int iter = 0; iter < Iteration; iter++)
            {
                for (int i = 0; i < Points.Length; i++)
                {
                    int j = 0;
                    f = rand.Next(1, 101);
                    if (f == 1)
                    {
                        do
                        {
                            Points[i] = Funcs[0].GetResult(Points[i]);
                            j++;
                            i++;
                        } while (j < k && i < Points.Length);
                    }
                    else if (f > 1 && f <= 86)
                    {
                        do
                        {
                            Points[i] = Funcs[1].GetResult(Points[i]);
                            j++;
                            i++;
                        } while (j < k && i < Points.Length);
                    }
                    else if (f > 86 && f <= 93)
                    {
                        do
                        {
                            Points[i] = Funcs[2].GetResult(Points[i]);
                            j++;
                            i++;
                        } while (j < k && i < Points.Length);
                    }
                    else
                    {
                        do
                        {
                            Points[i] = Funcs[3].GetResult(Points[i]);
                            j++;
                            i++;
                        } while (j < k && i < Points.Length);

                    }
                    k += j;
                }
                for (int i = 0; i < Points.Length; i++)
                {
                    g.FillRectangle(new SolidBrush(ColorOfPoint),Math.Abs(40*Points[i].Y+Center.Y/4)-40,Math.Abs(40*Points[i].X+Center.X/2), 2, 2);
                }
            }
        }
    }
}
