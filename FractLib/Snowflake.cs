﻿using System;
using System.Drawing;
using AppendixLibrary;

namespace FractLib
{
    public class Snowflake : Fractal
    {
        public  PointF[] Points { get; set; }
        public override Color ColorOfPoint { get; set; }
        public int Iteration { get; set; }
        public int Length { get; set; }
        public bool IsFill { get; set; }
        public bool Anti { get; set; }
        public Snowflake(PointF[] point,Color color,int iteration,int length,bool isfill,bool anti)
        {
            Points = point;
            ColorOfPoint = color;
            Iteration = iteration;
            Length = length;
            IsFill = isfill;
            Anti = anti;
        }
        public override void Building(Graphics g)
        {
            Vector n,mb;
            float x, y;
            
            for (int i = 1; i <= Iteration; i++)
            {
                Length /= 3;
                PointF[] pointscopy = new PointF[Points.Length];
                int r = 0;
                for (int j = 0; j < 3 * Math.Pow(4, i - 1); j++)
                {
                    pointscopy[r] = Points[j];
                    r++;
                    if (j != 3 * Math.Pow(4, i - 1)-1)
                    { 
                        x = (float)((Points[j].X + 0.5 * Points[j + 1].X) / 1.5);
                        y = (float)((Points[j].Y + 0.5 * Points[j + 1].Y) / 1.5);
                        pointscopy[r] = new PointF(x, y);
                        r++; 
                        x = (Points[j].X + 2 * Points[j + 1].X) / 3;
                        y = (Points[j].Y + 2 * Points[j + 1].Y) / 3;
                        pointscopy[r+1] = new PointF(x, y);
                        r++;
                        x=pointscopy[r].X-pointscopy[r-2].X;
                        y=-(pointscopy[r].Y-pointscopy[r-2].Y);
                        n = new Vector(new PointF(y,x));
                        mb = new Vector(Length / 2 * Math.Sqrt(3));
                        double lambda = mb.Modul / n.Modul;
                        mb.Coordinate = n * lambda;
                        pointscopy[r-1] = mb.FindEnd(new PointF((pointscopy[r-2].X+pointscopy[r].X)/2, (pointscopy[r - 2].Y + pointscopy[r].Y) / 2));
                        if(Anti==false)pointscopy[r - 1] = new PointF(pointscopy[r - 2].X + pointscopy[r].X - pointscopy[r - 1].X, pointscopy[r - 2].Y + pointscopy[r].Y - pointscopy[r - 1].Y);
                        r++;
                    }
                    else
                    {
                        x = (float)((pointscopy[r-1].X + 0.5 * pointscopy[0].X) / 1.5);
                        y = (float)((pointscopy[r-1].Y + 0.5 * pointscopy[0].Y) / 1.5);
                        pointscopy[r] = new PointF(x, y);
                        r++;
                        x = (pointscopy[r-2].X + 2 * pointscopy[0].X) / 3;
                        y = (pointscopy[r-2].Y + 2 * pointscopy[0].Y) / 3;
                        pointscopy[r+1] = new PointF(x, y);
                        r++;
                        x = pointscopy[r].X - pointscopy[r - 2].X;
                        y = -(pointscopy[r].Y - pointscopy[r - 2].Y);
                        n = new Vector(new PointF(y, x));
                        mb = new Vector(Length / 2 * Math.Sqrt(3));
                        double lambda = mb.Modul / n.Modul;
                        mb.Coordinate = n * lambda;
                        pointscopy[r - 1] = mb.FindEnd(new PointF((pointscopy[r - 2].X + pointscopy[r].X) / 2, (pointscopy[r - 2].Y + pointscopy[r].Y) / 2));
                        if (Anti == false) pointscopy[r - 1] = new PointF(pointscopy[r - 2].X + pointscopy[r].X - pointscopy[r - 1].X, pointscopy[r - 2].Y + pointscopy[r].Y - pointscopy[r - 1].Y);
                        r++;
                    }
                }
                pointscopy.CopyTo(Points, 0);
            }
            if(IsFill)g.FillPolygon(new SolidBrush(ColorOfPoint), Points);
            else g.DrawPolygon(new Pen(ColorOfPoint), Points);
        }
    }
}
