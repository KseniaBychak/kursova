﻿using System.Drawing;
using AppendixLibrary;

namespace FractLib
{
    public class JuliaSet:Fractal
    {
        public override Color ColorOfPoint { get; set; }
        public Complex C { get; set; }
        public Size SizeFract { get; set; }
        public JuliaSet(Color color,Complex c,Size size)
        {
            ColorOfPoint = color;
            C = c;
            SizeFract = size;
        }
        public override void Building(Graphics g)
        {
            Color color;
            for (float x = 0; x < SizeFract.Width; x++)
            {
                for (float y = 0; y < SizeFract.Height; y++)
                {
                    double a = -2 + x * 4.0 / SizeFract.Width;
                    double b = -2 + y * 4.0 / SizeFract.Height;
                    Complex z = new Complex(a,b);
                    for (int k = 0; k < 60; k++)
                    {
                        if (z.Norm() < 4)
                        {
                            color = Color.Black;
                            z = z * z + C;
                            if(z.Norm() >= 4) color = GetColor(k);
                            g.FillRectangle(new SolidBrush(color), x, y, 1, 1);
                        }
                        else break;
                    }
                }
            }
        }
        private Color  GetColor(int iteration)
        {
            int r = ColorOfPoint.R, g = ColorOfPoint.G, b = ColorOfPoint.B;
            for (int i = 0; i <= iteration; i++)
            {
                if (r + 20 <= 255) r += 20;
                else if (g + 20 <= 255) g += 20;
                else if (b + 20 <= 255) b += 20;
                else
                {
                    r =ColorOfPoint.R;
                    g =ColorOfPoint.G;
                    b =ColorOfPoint.B;
                }
            }
            return Color.FromArgb(r, g, b);
        }
    }
}
