﻿using System.Drawing;

namespace AppendixLibrary
{
    public class AfinFunc
    {
        public double[,] Matr { get; set; }
        public double[] Add { get; set; }
        public AfinFunc(double[,] matr, double[] add)
        {
            Matr = matr;
            Add = add;
        }
        public PointF GetResult(PointF point)
        {
            return new PointF((float)(Matr[0, 0] * point.X + Matr[0, 1] * point.Y + Add[0]), (float)(Matr[1, 0] * point.X + Matr[1, 1] * point.Y + Add[1]));
        }
    }
}
