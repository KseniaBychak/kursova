﻿using System;
using System.Drawing;

namespace AppendixLibrary
{
    public class Vector
    {
        public PointF StartPoint { get; set; }
        public PointF EndPoint { get; set; }
        public PointF Coordinate { get; set; }
        public double Modul { get; set; }
        public Vector(PointF start,PointF end)
        {
            StartPoint = start;
            EndPoint = end;
            Coordinate = new PointF(EndPoint.X - StartPoint.X, EndPoint.Y - StartPoint.Y);
            Modul = Math.Sqrt(Coordinate.X*Coordinate.X + Coordinate.Y*Coordinate.Y);
        }
        public Vector(PointF coordinate)
        {
            Coordinate =coordinate;
            Modul = Math.Sqrt(Coordinate.X * Coordinate.X + Coordinate.Y * Coordinate.Y);
        }
        public Vector(double modul)
        {
            Modul = modul;
        }
        static public PointF operator *(Vector v,double lambda)
        {
            return new PointF((float)(v.Coordinate.X * lambda), (float)(v.Coordinate.Y * lambda));
        }
        public PointF FindEnd(PointF start)
        {
            return new PointF(Coordinate.X+start.X,Coordinate.Y+start.Y);
        }
    }
}
