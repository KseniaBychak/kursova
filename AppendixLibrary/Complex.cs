﻿namespace AppendixLibrary
{
    public class Complex
    {
        public double Real { get; set; }
        public double Imaginary { get; set; }
        public Complex(double real,double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        static public Complex operator + (Complex a,Complex b)
        {
            return new Complex(a.Real + b.Real, a.Imaginary + b.Imaginary);
        }
        static public Complex operator *(Complex a, Complex b)
        {
            return new Complex(a.Real*b.Real -a.Imaginary*b.Imaginary, a.Real * b.Imaginary + a.Imaginary * b.Real);
        }
        public double Norm()
        {
            return Real*Real+Imaginary*Imaginary;
        }
    }
}
